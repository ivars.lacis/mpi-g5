# Saites

## Raksti

SQL Lite / Room tutorial:
- https://codelabs.developers.google.com/codelabs/android-room-with-a-view/#13

Working with Architecture Components: Room, LiveData, ViewModel:
- https://google-developer-training.github.io/android-developer-advanced-course-practicals/unit-6-working-with-architecture-components/lesson-14-room,-livedata,-viewmodel/14-1-a-room-livedata-viewmodel/14-1-a-room-livedata-viewmodel.html

Android Room: Handling Relations Using LiveData
- https://proandroiddev.com/android-room-handling-relations-using-livedata-2d892e40bd53

ItemTouchHelper : Limit swipe width of ItemTouchHelper.SimpleCallBack on RecyclerView
- https://stackoverflow.com/questions/41015101/itemtouchhelper-limit-swipe-width-of-itemtouchhelper-simplecallback-on-recycle


## Youtube

Android Spinner
- https://www.youtube.com/watch?v=5O070jDlOW8

Android Dialog Fragment to Activity
- https://www.youtube.com/watch?v=--dJm6z5b0s

RecyclerView OnClickListener (Best practice way)
- https://www.youtube.com/watch?v=69C1ljfDvl0

Dialog Fragment to Fragment
- https://www.youtube.com/watch?v=LUV_djRHSEY

*Navigation Drawer with Fragments Part 1 - MENU AND ACTIVITY THEME - Android Studio Tutorial 
- https://www.youtube.com/watch?v=fGcMLu1GJEc

*Navigation Drawer with Fragments Part 2 - LAYOUT AND HAMBURGER ICON - Android Studio Tutorial
- https://www.youtube.com/watch?v=zYVEMCiDcmY

*Navigation Drawer with Fragments Part 3 - HANDLING MENU ITEM CLICKS - Android Studio Tutorial
- https://www.youtube.com/watch?v=bjYstsO1PgI

Open a Fragment with an Animation + Communicate with Activity - Android Studio Tutorial
- https://www.youtube.com/watch?v=BMTNaPcPjdw

## Misc

Lietotnes krāsu tēma
- https://material.io/tools/color/#!/?view.left=0&view.right=0&primary.color=2A2A57&secondary.color=F57C00

Master Yoda ēnas
- https://www.clipart.email/clipart/master-yoda-silhouette-118251.html

UML
- https://dbdiagram.io/d