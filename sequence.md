```mermaid
sequenceDiagram
participant FTra as FragmentTransactions
participant ANTra as ActivityNewTransaction
FTra->>ANTra: startActivityForResult(Intent, int);
activate ANTra
ANTra-->>FTra: setResult(int, Intent);
deactivate ANTra
```