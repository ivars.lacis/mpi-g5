package org.mpi.mpi.utils;

import org.mpi.mpi.R;

public enum Direction {
    IN("Ienākumi", 1), OUT("Izdevumi", -1);

    String display;
    int value;

    Direction(String display, int value) {
        this.display = display;
        this.value = value;
    }

    @Override public String toString() {
        return this.display;
    }

    public String getDisplay() {
        return this.display;
    }

    public int getValue() {
        return this.value;
    }

    public int getIcon() {
        int icon = R.drawable.ic_unknown;

        switch (this) {
            case IN:
                icon = R.drawable.ic_trending_up_black_24dp;
                break;
            case OUT:
                icon = R.drawable.ic_trending_down_black_24dp;
                break;
        }

        return icon;
    }

    public static Direction fromDisplay(String display) {
        if (display != null) {
            for (Direction dir : Direction.values()) {
                if (display.equalsIgnoreCase(dir.getDisplay())) {
                    return dir;
                }
            }
        }
        throw new IllegalArgumentException(display + " not found");
    }

    public static Direction fromValue(int value) {
        for (Direction dir : Direction.values()) {
            if (value == dir.value) {
                return dir;
            }
        }
        throw new IllegalArgumentException(value + " not found");
    }
}
