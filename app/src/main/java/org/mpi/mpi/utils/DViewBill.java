package org.mpi.mpi.utils;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import org.mpi.mpi.R;
import org.mpi.mpi.db.types.Account;
import org.mpi.mpi.views.ListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DViewBill extends ADialog {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mDialogView = inflater.inflate(R.layout.dialog_view_bill, container, false);

        if (getArguments() != null && getArguments().getString("PATH") != null) {
            String img = getArguments().getString("PATH");
            ImageView imageView = mDialogView.findViewById(R.id.dialog_view_bill_image);

            Uri uri = Uri.parse(img);
            imageView.setImageURI(uri);

        }

        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void dialogReturn() {

    }
}
