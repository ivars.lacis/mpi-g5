package org.mpi.mpi.utils;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import org.mpi.mpi.R;
import org.mpi.mpi.db.types.Account;
import org.mpi.mpi.views.ListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DNewAccount extends ADialog {

    private EditText mAccountName;
    private Spinner mSpinDirection;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mDialogView = inflater.inflate(R.layout.dialog_new_account, container, false);
        mAccountName = mDialogView.findViewById(R.id.dialog_account_name);

        mSpinDirection = mDialogView.findViewById(R.id.dialog_account_direction);

        ListAdapter<Map<String, Integer>> adapter = new ListAdapter<>(getContext());
        Map<String, Integer> map;
        List<Map<String, Integer>> list = new ArrayList<>();

        map = new HashMap<>();
        map.put("In", 1);
        list.add(map);

        map = new HashMap<>();
        map.put("Out", -1);
        list.add(map);

        adapter.setItems(list);


        ArrayAdapter<Direction> ad = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, Direction.values());

        mSpinDirection.setAdapter(ad);


        btnCancel = mDialogView.findViewById(R.id.button_cancel);
        btnOK = mDialogView.findViewById(R.id.button_ok);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void dialogReturn() {
        String result = mAccountName.getText().toString();
        Account a = new Account(result);

        int direction = ((Direction)mSpinDirection.getSelectedItem()).value;

        a.setDirection(direction);

//        if (!result.equals("")) {
//        if (a != null) {
            mDialogResultListener.receiveDialogResult(a);
//        }
    }
}
