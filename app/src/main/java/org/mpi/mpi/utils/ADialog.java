package org.mpi.mpi.utils;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public abstract class ADialog extends DialogFragment {
    private static final String TAG = "";

    public interface IDialogResultListener {  void receiveDialogResult(Object result); }
    public IDialogResultListener mDialogResultListener;

    View mDialogView;
    protected Button btnOK, btnCancel;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (btnCancel != null) {
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "dismiss dialog");
                    getDialog().dismiss();
                }
            });
        }

        if (btnOK != null) {
            btnOK.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "dialog ok");
                    dialogReturn();
                    getDialog().dismiss();
                }
            });
        }

        return mDialogView;
    }

    public abstract void dialogReturn();

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            if (getTargetFragment() == null) {
                mDialogResultListener = (IDialogResultListener) context;
            } else {
                mDialogResultListener = (IDialogResultListener) getTargetFragment();
            }
        } catch (ClassCastException e) {
            Log.e(TAG, "onAttach.ClassCastException e=" + e.getMessage());
        }
    }
}
