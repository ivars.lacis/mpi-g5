package org.mpi.mpi;

import android.app.DatePickerDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;

import org.mpi.mpi.db.repositories.TransactionRepository;
import org.mpi.mpi.db.types.Account;
import org.mpi.mpi.db.types.Transaction;
import org.mpi.mpi.utils.ADialog;
import org.mpi.mpi.utils.DSnapBill;
import org.mpi.mpi.views.AccountViewModel;
import org.mpi.mpi.views.ListAdapter;

import java.io.File;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;


public class ActivityNewTransaction extends AppCompatActivity implements ADialog.IDialogResultListener {
    private static class PostTransaction extends AsyncTask<Transaction, Void, Void> {
        public Void doInBackground(Transaction... params) {
            try {
                URL url = new URL("http://192.168.1.68:8081/api/tra");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                Log.d(TAG, "PostData: connection opened");


                int responseCode = -1;
                if (conn != null) {
                    conn.setDoOutput(true);
                    conn.setUseCaches(false);
                    conn.setReadTimeout(15000);
                    conn.setConnectTimeout(15000);
                    conn.setRequestMethod("POST");

                    conn.setRequestProperty("Content-Type","application/json");

                    conn.connect();

                    Gson json = new Gson();
                    OutputStreamWriter out = new   OutputStreamWriter(conn.getOutputStream());
                    out.write(json.toJson(params[0]));
                    out.close();

                    responseCode = conn.getResponseCode();
                }

                Log.d(TAG, "Response code: " + responseCode);



            } catch (Exception e) {
                Log.d(TAG, "Exception: " + e.getMessage() + "; " + e.toString());
            }

            return null;
        }
    }


    private static final String TAG = "ActivityNewTransaction";

    public static final int SNAP_BILL = 1;


    Spinner spinAccountFrom;
    Button btnDiscard, btnSave, btnSetDate, btnSnapBill;
    EditText txtAmount, txtDescription;
    TextView viewTraDate;
    Calendar mCalendar;
    DatePickerDialog.OnDateSetListener mDateSetListener;

    AccountViewModel mAccountViewModel;
    ListAdapter<Account> mAdapter;

    Account accountFrom;
    File mSnappedFile;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_transaction);

        mCalendar = Calendar.getInstance();



        spinAccountFrom = findViewById(R.id.spin_account_from);


        txtAmount = findViewById(R.id.ant_amount);
        txtAmount.setInputType(0x00002002);

        txtDescription = findViewById(R.id.ant_description);

        viewTraDate = findViewById(R.id.txt_tra_date);
        displaySelectedDate();

        btnSetDate = findViewById(R.id.ant_set_date);

        btnSave = findViewById(R.id.ant_save);
        btnDiscard = findViewById(R.id.ant_discard);

        btnSnapBill = findViewById(R.id.ant_snap_bill);

        mAdapter = new ListAdapter<>(ActivityNewTransaction.this);
        mAccountViewModel = ViewModelProviders.of(ActivityNewTransaction.this).get(AccountViewModel.class);
        mAccountViewModel.getAllAccounts().observe(ActivityNewTransaction.this, new Observer<List<Account>>() {
            @Override
            public void onChanged(@Nullable final List<Account> accounts) {
                mAdapter.setItems(accounts);
            }
        });

        spinAccountFrom.setAdapter(mAdapter);

        btnSetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog dialog = new DatePickerDialog(
                        ActivityNewTransaction.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth, // .Theme_DeviceDefault_Dialog_MinWidth,
                        mDateSetListener,
                        mCalendar.get(Calendar.YEAR),
                        mCalendar.get(Calendar.MONTH),
                        mCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                mCalendar.set(Calendar.YEAR, year);
                mCalendar.set(Calendar.MONTH, month);
                mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                displaySelectedDate();
            }
        };

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveTransaction();
                backToOverview(RESULT_OK);
            }
        });

        btnDiscard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToOverview(RESULT_CANCELED);
            }
        });

        btnSnapBill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DSnapBill dialog = new DSnapBill();
                dialog.show(getSupportFragmentManager(), "Snap");
            }
        });

    }

    private void saveTransaction() {
        accountFrom = (Account) spinAccountFrom.getSelectedItem();

        Double amount = Double.parseDouble(txtAmount.getText().toString());
        Transaction tra = new Transaction(amount);

        tra.setDescription(txtDescription.getText().toString());
        tra.setTransactionDate(mCalendar.getTime());

        if (mSnappedFile != null)
            tra.setBillImage(mSnappedFile.getAbsolutePath());

        if (accountFrom != null)
            tra.setAccountFrom(accountFrom.getAccountID());


        TransactionRepository traRepo = new TransactionRepository(getApplication());
        traRepo.insert(tra);

        new PostTransaction().execute(tra);
    }

    private void displaySelectedDate() {
        String formattedDate = (new SimpleDateFormat("yyyy-MM-dd")).format(mCalendar.getTime());
        viewTraDate.setText(formattedDate);
    }

    private void backToOverview(int result) {
        Intent replyIntent = new Intent();
        setResult(result, replyIntent);
        finish();
    }

    @Override
    public void receiveDialogResult(Object result) {
        try {
            mSnappedFile = (File) result;
        } catch (ClassCastException e) {
            Log.e(TAG, "receiveDialogResult.ClassCastException e=" + e.getMessage());
        }
    }



}
