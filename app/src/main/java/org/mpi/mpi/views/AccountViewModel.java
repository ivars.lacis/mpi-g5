package org.mpi.mpi.views;


import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import org.mpi.mpi.db.repositories.AccountRepository;
import org.mpi.mpi.db.types.Account;

import java.util.List;

public class AccountViewModel extends AndroidViewModel {
    private AccountRepository repo;
    private LiveData<List<Account>> allAccounts;

    public AccountViewModel (Application application) {
        super(application);
        repo = new AccountRepository(application);
        allAccounts = repo.getAllAccounts();
    }

    public LiveData<List<Account>> getAllAccounts() {
        return allAccounts;
    }

    public void insert (Account account) {
        repo.insert(account);
    }

    public void delete (Account account) {
        repo.delete(account);
    }
}