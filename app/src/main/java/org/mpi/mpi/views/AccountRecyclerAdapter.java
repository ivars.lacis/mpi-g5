package org.mpi.mpi.views;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.mpi.mpi.R;
import org.mpi.mpi.db.types.Account;
import org.mpi.mpi.helpers.RecyclerItemTouchHelper;
import org.mpi.mpi.utils.Direction;

import java.text.NumberFormat;
import java.util.List;

public class AccountRecyclerAdapter extends RecyclerView.Adapter<AccountRecyclerAdapter.AccountViewHolder> {
    private static final String TAG = "AccountRecyclerAdapter";
    private final LayoutInflater inflater;
    private List<Account> accounts;

    public class AccountViewHolder extends RecyclerView.ViewHolder implements RecyclerItemTouchHelper.IViewHolder {
        public final TextView name, balance;
        public final ImageView directionIcon;
        ConstraintLayout viewBackground;
        LinearLayout viewForeground;

        public AccountViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.txt_account_name);
            balance = itemView.findViewById(R.id.txt_account_balance);
            directionIcon = itemView.findViewById(R.id.ico_account_direction);
            viewBackground = itemView.findViewById(R.id.view_background);
            viewForeground = itemView.findViewById(R.id.view_foreground);
        }

        @Override
        public ConstraintLayout getBackground() {
            return this.viewBackground;
        }

        @Override
        public LinearLayout getForeground() {
            return this.viewForeground;
        }
    }

    public AccountRecyclerAdapter(Context context) {
        inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public AccountViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.ritem_account_frame, parent, false);
        return new AccountViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final AccountViewHolder holder, int position) {
        if (accounts != null) {
            final Account current = accounts.get(position);


            holder.name.setText(current.getName());

            String str = NumberFormat.getCurrencyInstance().format(current.getBalance());
            holder.balance.setText(str);


            Direction dir = Direction.fromValue(current.getDirection());
            holder.directionIcon.setImageResource(dir.getIcon());


        } else {
            holder.name.setText("Konti neeksistē");
        }
    }

    @Override
    public int getItemCount() {
        if (accounts != null)
            return accounts.size();
        else return 0;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
        notifyDataSetChanged();
    }

    public void removeAccount(int position) {
        if (accounts != null)
            accounts.remove(position);

        notifyItemRemoved(position);
    }

    public void restoreItem(Account account, int position) {
        if (accounts != null)
            accounts.add(position, account);

        notifyItemInserted(position);
    }
}
