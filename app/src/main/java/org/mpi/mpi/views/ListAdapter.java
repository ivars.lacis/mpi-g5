package org.mpi.mpi.views;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.mpi.mpi.R;

import java.util.ArrayList;
import java.util.List;


public class ListAdapter<T> extends BaseAdapter {
    private static final String TAG = "ListAdapter";

    private final LayoutInflater inflater;
    private List<T> mItems;

    class ViewHolder {
        public final TextView name;

        ViewHolder(View itemView) {
            name = itemView.findViewById(R.id.txt_description);
        }

        void setNameText(String text) {
            name.setText(text);
        }
    }

    public ListAdapter(Context context) {
        inflater = LayoutInflater.from(context);
        mItems = new ArrayList<>();
    }

    public void setItems(List<T> items) {
        mItems = items;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View itemView = convertView;

        if (itemView == null) {
            itemView = inflater.inflate(R.layout.list_row, parent, false);

            viewHolder = new ViewHolder(itemView);
            viewHolder.setNameText(mItems.get(position).toString());
        } else {
            viewHolder = new ViewHolder(itemView);
            viewHolder.setNameText(mItems.get(position).toString());
        }

        itemView.setTag(viewHolder);

        return itemView;
    }
}
