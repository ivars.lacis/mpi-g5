package org.mpi.mpi.views;


import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.ComputableLiveData;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.support.annotation.NonNull;
import android.util.Log;

import org.mpi.mpi.db.repositories.TransactionRepository;
import org.mpi.mpi.db.types.Transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TransactionViewModel extends AndroidViewModel {
    private static final String TAG = "TransactionViewModel";

    private TransactionRepository repo;
    private LiveData<List<Transaction>> allTransactions;

    public TransactionViewModel (Application application) {
        super(application);
        repo = new TransactionRepository(application);
        allTransactions = repo.getAllTransactions();
    }

    public LiveData<List<Transaction>> getAllTransactions() {
        return allTransactions;
    }

    public LiveData<List<Transaction>> getTransactionsByDate(final Date sdate, Date edate) {
//        return new ComputableLiveData<List<Transaction>>() {
//            @Override
//            protected List<Transaction> compute() {
//                List<Transaction> collection = new ArrayList<>();
//
//                collection.add(
//                        allTransactions.getValue().stream().anyMatch(tra -> tra.getTransactionDate().equals(sdate))
//                );
//                return collection;
//            }
//
//            @Override
//            protected void finalize() {
//
//            }
//        }.getLiveData();
        return allTransactions;
    }

    public void insert (Transaction transaction) {
        repo.insert(transaction);
    }

    public void delete (Transaction transaction) {
        repo.delete(transaction);
    }
}