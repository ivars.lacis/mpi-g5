package org.mpi.mpi.views;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.mpi.mpi.R;
import org.mpi.mpi.db.types.Transaction;
import org.mpi.mpi.helpers.RecyclerItemTouchHelper;
import org.mpi.mpi.utils.DSnapBill;
import org.mpi.mpi.utils.DViewBill;
import org.mpi.mpi.utils.Direction;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.List;

public class TransactionRecyclerAdapter extends RecyclerView.Adapter<TransactionRecyclerAdapter.TransactionViewHolder> {
    private final LayoutInflater inflater;
    private final Fragment parent;
    private List<Transaction> transactions;

    public class TransactionViewHolder extends RecyclerView.ViewHolder implements RecyclerItemTouchHelper.IViewHolder {
        public final TextView amount, date, description;
        public final ImageButton viewBill;
        ImageView directionicon;
        ConstraintLayout viewBackground;
        LinearLayout viewForeground;


        public TransactionViewHolder(final View itemView) {
            super(itemView);

            directionicon = itemView.findViewById(R.id.ico_transaction_direction);
            amount = itemView.findViewById(R.id.txt_transaction_amount);
            date = itemView.findViewById(R.id.txt_transaction_date);
            description = itemView.findViewById(R.id.txt_transaction_description);
            viewBill = itemView.findViewById(R.id.view_bill);

            viewBackground = itemView.findViewById(R.id.view_background);
            viewForeground = itemView.findViewById(R.id.view_foreground);
        }

        @Override
        public ConstraintLayout getBackground() {
            return this.viewBackground;
        }

        @Override
        public LinearLayout getForeground() {
            return this.viewForeground;
        }
    }

    public TransactionRecyclerAdapter(Fragment parent, Context context) {
        this.parent = parent;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public TransactionRecyclerAdapter.TransactionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.ritem_transaction_frame, parent, false);
        return new TransactionRecyclerAdapter.TransactionViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final TransactionRecyclerAdapter.TransactionViewHolder holder, int position) {
        if (transactions != null) {
            final Transaction current = transactions.get(position);

            String str = NumberFormat.getCurrencyInstance().format(current.getAmount());
            holder.amount.setText(str);


            holder.directionicon.setImageResource(current.getDirection().getIcon());

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = formatter.format(current.getTransactionDate());
            holder.date.setText(formattedDate);

            holder.description.setText(current.getDescription());

            if (current.getBillImage() != null) {
                holder.viewBill.setVisibility(ImageButton.VISIBLE);

                holder.viewBill.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DViewBill viewBill = new DViewBill();

                        Bundle args = new Bundle();
                        args.putString("PATH", current.getBillImage());

                        viewBill.setArguments(args);
                        viewBill.show(parent.getChildFragmentManager(), "Bill");
                    }
                });
            }


            switch (current.getDirection()) {
                case OUT:
                    holder.amount.setTextColor(Color.RED);
                    holder.date.setTextColor(Color.RED);
                    holder.description.setTextColor(Color.RED);
                    holder.directionicon.setColorFilter(Color.RED);
                    break;
                case IN:
                    holder.amount.setTextColor(Color.BLACK);
                    holder.date.setTextColor(Color.BLACK);
                    holder.description.setTextColor(Color.BLACK);
                    holder.directionicon.setColorFilter(Color.BLACK);
                    break;
            }

        } else {
            holder.description.setText("darījumu nav");
        }
    }

    @Override
    public int getItemCount() {
        if (transactions != null)
            return transactions.size();
        else return 0;
    }


    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
        notifyDataSetChanged();
    }

    public void removeTransactions(int position) {
        if (transactions != null)
            transactions.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreTransaction(Transaction transaction, int position) {
        if (transactions != null)
            transactions.add(position, transaction);
        notifyItemInserted(position);
    }
}
