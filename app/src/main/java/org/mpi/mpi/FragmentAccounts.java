package org.mpi.mpi;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.mpi.mpi.db.types.Account;
import org.mpi.mpi.db.types.Transaction;
import org.mpi.mpi.helpers.RecyclerItemTouchHelper;
import org.mpi.mpi.utils.DNewAccount;
import org.mpi.mpi.utils.ADialog;
import org.mpi.mpi.views.AccountRecyclerAdapter;
import org.mpi.mpi.views.AccountViewModel;

import java.text.NumberFormat;
import java.util.List;

public class FragmentAccounts extends Fragment
        implements RecyclerItemTouchHelper.ITouchHelperListener, ADialog.IDialogResultListener {
    private static final String TAG = "FragmentAccounts";

    private AccountViewModel mAccountViewModel;
    private AccountRecyclerAdapter mAdapter;
    private TextView mTotalBalance;
    public static final int NEW_ACCOUNT_ACTIVITY_REQUEST_CODE = 1;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstancState) {

        View view = inflater.inflate(R.layout.fragment_accounts, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_transactions);
        mAdapter = new AccountRecyclerAdapter(this.getContext());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        DividerItemDecoration div;
        switch (getResources().getConfiguration().orientation) {
            case 1:
                div = new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL);
                break;
            case 2:
                div = new DividerItemDecoration(view.getContext(), DividerItemDecoration.HORIZONTAL);
                break;
            default:
                div = new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL);
        }
        recyclerView.addItemDecoration(div);


        Log.d(TAG, "onCreateView: orientation = " + getResources().getConfiguration().orientation);

        ItemTouchHelper.SimpleCallback swipeLeft = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(swipeLeft).attachToRecyclerView(recyclerView);

//        ItemTouchHelper.SimpleCallback swipeRight = new RecyclerItemTouchHelper(0, ItemTouchHelper.RIGHT, this);
//        new ItemTouchHelper(swipeRight).attachToRecyclerView(recyclerView);

        mTotalBalance = view.findViewById(R.id.total_balance);
        mAccountViewModel = ViewModelProviders.of(FragmentAccounts.this).get(AccountViewModel.class);
        mAccountViewModel.getAllAccounts().observe(FragmentAccounts.this, new Observer<List<Account>>() {
            @Override
            public void onChanged(@Nullable final List<Account> accounts) {
                mAdapter.setAccounts(accounts);

                double totalBalance = 0;
                if (accounts != null) {
                    for (Account account : accounts) {
                        int dir = account.getDirection();

                        List<Transaction> transactions = account.getTransactions();

                        if (transactions != null) {
                            for (Transaction transaction : transactions) {
                                totalBalance += (dir * transaction.getAmount());
                            }
                        }
                    }
                }
                String str = NumberFormat.getCurrencyInstance().format(totalBalance);
                mTotalBalance.setText(str);
            }
        });


        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DNewAccount dialog = new DNewAccount();
                dialog.setTargetFragment(FragmentAccounts.this, NEW_ACCOUNT_ACTIVITY_REQUEST_CODE);
                dialog.show(getFragmentManager(), "Accounts Dialog");

            }
        });

        return view;
    }


    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        Log.d(TAG, "onSwiped: swipe direction: " + direction);

        if (viewHolder instanceof AccountRecyclerAdapter.AccountViewHolder) {
            switch (direction) {
                case ItemTouchHelper.LEFT :
                    Account i = mAccountViewModel.getAllAccounts().getValue().get(viewHolder.getAdapterPosition());

                    mAdapter.removeAccount(viewHolder.getAdapterPosition());

                    mAccountViewModel.delete(i);

                    Toast.makeText(this.getContext(), "Ieraksts dzēsts", Toast.LENGTH_LONG).show();
                    break;
                case ItemTouchHelper.RIGHT:
                    Toast.makeText(this.getContext(), "Edit Account", Toast.LENGTH_LONG).show();
                    break;
            }

        }
    }

    @Override
    public void receiveDialogResult(Object result) {
        if (result != null) {
            Account account = (Account)result;
            mAccountViewModel.insert(account);
        }
    }
}