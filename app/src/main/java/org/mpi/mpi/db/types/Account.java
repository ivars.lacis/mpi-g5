package org.mpi.mpi.db.types;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

@Entity(tableName = "accounts")
public class Account {
    private static final String TAG = "Account";

    @Ignore
    private List<Transaction> transactions;



    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int accountID;

    @NonNull
    @ColumnInfo(name = "name")
    private String name;

    @ColumnInfo(name = "balance")
    private double balance = 0;

    @ColumnInfo(name = "direction")
    private int direction;


    public Account(@NonNull String name) {
        this.name = name;
    }


    public int getAccountID() {
        return accountID;
    }

    public void setAccountID(int accountID) {
        this.accountID = accountID;
    }

    public String getName() {
        return name;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String toString() {
        Log.d(TAG, "toString: " + name);
        return name;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }
}
