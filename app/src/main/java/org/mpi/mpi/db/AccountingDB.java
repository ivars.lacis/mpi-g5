package org.mpi.mpi.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import org.mpi.mpi.db.types.Account;
import org.mpi.mpi.db.types.Schedule;
import org.mpi.mpi.db.types.Transaction;


@Database(entities = {Account.class, Transaction.class, Schedule.class}, version = 2)
public abstract class AccountingDB extends RoomDatabase {
    private static volatile AccountingDB INSTANCE;

    public abstract IDAO dao();

//    public abstract ITransactionDAO transactionDAO();


    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE transactions ADD bill_img VARCHAR");
        }
    };

//    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE transactions ADD tra_date INTEGER");
//        }
//    };

//    static final Migration MIGRATION_3_4 = new Migration(3, 4) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//
//        }
//    };

//    static final Migration MIGRATION_4_5 = new Migration(4, 5) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE transactions ADD description VARCHAR");
//        }
//    };

//    static final Migration MIGRATION_5_6 = new Migration(5, 6) {
//        @Override
//        public void migrate(SupportSQLiteDatabase database) {
//            database.execSQL("ALTER TABLE accounts ADD direction INTEGER NOT NULL DEFAULT 1");
//        }
//    };


    public static AccountingDB getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AccountingDB.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AccountingDB.class, "accounting")
                            .addMigrations(MIGRATION_1_2)
//                            .addMigrations(MIGRATION_2_3)
//                            .addMigrations(MIGRATION_3_4)
//                            .addMigrations(MIGRATION_4_5)
//                            .addMigrations(MIGRATION_5_6)
//                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }

        return INSTANCE;
    }


//    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback() {
//        @Override
//        public void onOpen(@NonNull SupportSQLiteDatabase db) {
//            super.onOpen(db);
//            new AccountingDB.PopulateDbAsync(INSTANCE).execute();
//        }
//    };

//    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {
//
//        private final IDAO dao;
//
//        PopulateDbAsync(AccountingDB db) {
//            dao = db.dao();
//        }
//
//        @Override
//        protected Void doInBackground(final Void... params) {
//            dao.deleteAllAccounts();
//            Account account = new Account("Alga");
//            dao.setAccount(account);
//            account = new Account("Izdevumi");
//            dao.setAccount(account);
//
//            return null;
//        }
//    }
}
