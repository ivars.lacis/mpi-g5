package org.mpi.mpi.db.repositories;

import android.app.Application;
import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.Transformations;
import android.os.AsyncTask;
import android.support.annotation.Nullable;

import org.mpi.mpi.db.IDAO;
import org.mpi.mpi.db.AccountingDB;
import org.mpi.mpi.db.types.Account;
import org.mpi.mpi.db.types.Transaction;


import java.util.List;

public class AccountRepository {
    private static final String TAG = "AccountRepository";

    private IDAO dao;
//    private LiveData<List<Account>> allAccounts;

    public AccountRepository(Application app) {
        AccountingDB db = AccountingDB.getDatabase(app);

        dao = db.dao();
//        allAccounts = dao.getAllAccounts();
    }

    public LiveData<List<Account>> getAllAccounts() {
        LiveData<List<Account>> accountsLD = dao.getAllAccounts();
        return fetchRelated(accountsLD);

    }


    public void insert(Account account) {
        new insertAsyncTask(dao).execute(account);
    }

    public void delete(Account account) {
        new deleteAsyncTask(dao).execute(account);
    }


    private LiveData<List<Account>> fetchRelated(LiveData<List<Account>> source) {
        return Transformations.switchMap(source, new Function<List<Account>, LiveData<List<Account>>>() {
            @Override
            public LiveData<List<Account>> apply(final List<Account> inputAccounts) {
                final MediatorLiveData<List<Account>> accountMediatorLD = new MediatorLiveData<>();
                for (final Account account : inputAccounts) {
                    accountMediatorLD.addSource(dao.getTransactionByAccountLD(account.getAccountID()), new Observer<List<Transaction>>() {
                        @Override
                        public void onChanged(@Nullable List<Transaction> transactions) {
                            double traSum = 0;
                            if (transactions != null) {
                                for (Transaction tra : transactions) {
                                    traSum += tra.getAmount();
                                }
                            }
                            account.setBalance(traSum);
                            account.setTransactions(transactions);
                            accountMediatorLD.postValue(inputAccounts);
                        }
                    });
                }
                return accountMediatorLD;
            }
        });
    }

    private static class insertAsyncTask extends AsyncTask<Account, Void, Void> {
        private IDAO dao;

        insertAsyncTask(IDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(final Account... params) {
            dao.setAccount(params[0]);

            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Account, Void, Void> {
        private IDAO dao;

        deleteAsyncTask(IDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(final Account... params) {
            dao.deleteAccount(params[0]);

            return null;
        }
    }
}
