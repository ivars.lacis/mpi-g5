package org.mpi.mpi.db;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

public class ConverterDate {
    @TypeConverter
    public static Date toDate(Long date) {
        return date == null ? null : new Date(date);
    }

    @TypeConverter
    public static Long toLong(Date date) {
        return date == null ? null : date.getTime();
    }
}