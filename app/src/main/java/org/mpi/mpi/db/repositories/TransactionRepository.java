package org.mpi.mpi.db.repositories;

import android.app.Application;
import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.Transformations;
import android.os.AsyncTask;
import android.support.annotation.Nullable;

import org.mpi.mpi.db.AccountingDB;
import org.mpi.mpi.db.IDAO;
import org.mpi.mpi.db.types.Account;
import org.mpi.mpi.db.types.Transaction;
import org.mpi.mpi.utils.Direction;

import java.util.Date;
import java.util.List;

public class TransactionRepository {
    private IDAO dao;

    public TransactionRepository(Application app) {
        AccountingDB db = AccountingDB.getDatabase(app);
        dao = db.dao();
    }

    public LiveData<List<Transaction>> getAllTransactions() {
        LiveData<List<Transaction>> transactionsLD = dao.getAllTransactions();
        return fetchRelated(transactionsLD);
    }


    public LiveData<List<Transaction>> getTransactionsByDate(Date sdate, Date edate) {
        LiveData<List<Transaction>> traLD = dao.getTransactionsByDate(sdate, edate);
        return fetchRelated(traLD);
    }

    public void insert(Transaction transaction) {
        new TransactionRepository.insertAsyncTask(dao).execute(transaction);
    }

    public void delete(Transaction transaction) {
        new TransactionRepository.deleteAsyncTask(dao).execute(transaction);
    }


    private LiveData<List<Transaction>> fetchRelated(LiveData<List<Transaction>> source) {
        return Transformations.switchMap(source, new Function<List<Transaction>, LiveData<List<Transaction>>>() {
            @Override
            public LiveData<List<Transaction>> apply(final List<Transaction> inputTransactions) {
                final MediatorLiveData<List<Transaction>> transactionsMediatorLD = new MediatorLiveData<>();

                for (final Transaction tra : inputTransactions) {
                    transactionsMediatorLD.addSource(dao.getAccountById(tra.getAccountFrom()), new Observer<Account>() {
                        @Override
                        public void onChanged(@Nullable Account account) {
                            if (account != null) {
                                tra.setDirection(Direction.fromValue(account.getDirection()));
                            }
                            transactionsMediatorLD.postValue(inputTransactions);
                        }
                    });
                }
                return transactionsMediatorLD;
            }
        });
    }

    private static class insertAsyncTask extends AsyncTask<Transaction, Void, Void> {
        private IDAO dao;

        insertAsyncTask(IDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(final Transaction... params) {
            dao.setTransaction(params[0]);

            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Transaction, Void, Void> {
        private IDAO dao;

        deleteAsyncTask(IDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(final Transaction... params) {
            dao.deleteTransaction(params[0]);

            return null;
        }
    }
}
