package org.mpi.mpi.db.repositories;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import org.mpi.mpi.db.AccountingDB;
import org.mpi.mpi.db.IDAO;
import org.mpi.mpi.db.types.Schedule;

import java.util.List;

public class ScheduleRepository {
    private static final String TAG = "ScheduleRepository";

    private IDAO dao;

    public ScheduleRepository(Application app) {
        AccountingDB db = AccountingDB.getDatabase(app);
        dao = db.dao();
    }

    public LiveData<List<Schedule>> getAllSchedules() {
        return dao.getAllSchedules();

    }


    public void insert(Schedule schedule) {
        new ScheduleRepository.insertAsyncTask(dao).execute(schedule);
    }

    public void delete(Schedule schedule) {
        new ScheduleRepository.deleteAsyncTask(dao).execute(schedule);
    }


    private static class insertAsyncTask extends AsyncTask<Schedule, Void, Void> {
        private IDAO dao;

        insertAsyncTask(IDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(final Schedule... params) {
            dao.setSchedule(params[0]);
            return null;
        }
    }

    private static class deleteAsyncTask extends AsyncTask<Schedule, Void, Void> {
        private IDAO dao;

        deleteAsyncTask(IDAO dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(final Schedule... params) {
            dao.deleteSchedule(params[0]);

            return null;
        }
    }
}
