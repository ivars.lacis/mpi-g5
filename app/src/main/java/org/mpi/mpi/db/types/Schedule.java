package org.mpi.mpi.db.types;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import org.mpi.mpi.db.ConverterDate;

import java.util.Date;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "schedule",
        foreignKeys = @ForeignKey(
                entity = Account.class,
                parentColumns = "id",
                childColumns = "account_from",
                onDelete = CASCADE)
)
@TypeConverters(ConverterDate.class)
public class Schedule {
    private static final String TAG = "Schedule";

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int scheduleId;

    @ColumnInfo(name = "account_from")
    private int account;

    @ColumnInfo(name = "enabled")
    private int enabled;

    @ColumnInfo(name = "amount")
    private double amount;

    @ColumnInfo(name = "next")
    private Date nextRunDate;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "rule")
    private String scheduleRule;


    public int getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }

    public int getAccount() {
        return account;
    }

    public void setAccount(int account) {
        this.account = account;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getNextRunDate() {
        return nextRunDate;
    }

    public void setNextRunDate(Date nextRunDate) {
        this.nextRunDate = nextRunDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getScheduleRule() {
        return scheduleRule;
    }

    public void setScheduleRule(String scheduleRule) {
        this.scheduleRule = scheduleRule;
    }
}
