package org.mpi.mpi.db.types;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;

import static android.arch.persistence.room.ForeignKey.CASCADE;

import org.mpi.mpi.db.ConverterDate;
import org.mpi.mpi.utils.Direction;

import java.util.Date;


@Entity(tableName = "transactions",
        foreignKeys = @ForeignKey(
                entity = Account.class,
                parentColumns = "id",
                childColumns = "account_from",
                onDelete = CASCADE
        )
)
@TypeConverters(ConverterDate.class)
public class Transaction {


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int transactionID;

    @ColumnInfo(name = "transaction_amount")
    private double amount;

    @ColumnInfo(name = "account_from")
    private int accountFrom;

    @ColumnInfo(name = "account_to")
    private int accountTo;

    @ColumnInfo(name = "tra_date")
    private Date transactionDate;

    @ColumnInfo(name = "description")
    private String description;

    @ColumnInfo(name = "bill_img")
    private String billImage;

    @Ignore
    Direction direction;


    public Transaction(double amount) {
        this.amount = amount;
        this.transactionDate = new Date();
    }


    public int getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(int transactionID) {
        this.transactionID = transactionID;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getAccountFrom() {
        return accountFrom;
    }

    public void setAccountFrom(int accountFrom) {
        this.accountFrom = accountFrom;
    }

    public int getAccountTo() {
        return accountTo;
    }

    public void setAccountTo(int accountTo) {
        this.accountTo = accountTo;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }

    public String getBillImage() {
        return billImage;
    }

    public void setBillImage(String billImage) {
        this.billImage = billImage;
    }
}
