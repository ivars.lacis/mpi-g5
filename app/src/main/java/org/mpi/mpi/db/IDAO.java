package org.mpi.mpi.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.TypeConverters;

import org.mpi.mpi.db.types.Account;
import org.mpi.mpi.db.types.Schedule;
import org.mpi.mpi.db.types.Transaction;

import java.util.Date;
import java.util.List;

@Dao
public interface IDAO {
    @Insert
    void setAccount(Account account);

    @Delete
    void deleteAccount(Account account);

    @Query("DELETE FROM accounts")
    void deleteAllAccounts();

    @Query("SELECT * FROM accounts ORDER BY lower(name) ASC")
    LiveData<List<Account>> getAllAccounts();

    @Query("SELECT * FROM accounts WHERE id = :accountId")
    LiveData<Account> getAccountById(int accountId);



    @Insert
    void setTransaction(Transaction transaction);

    @Delete
    void deleteTransaction(Transaction transaction);

    @Query("DELETE FROM transactions")
    void deleteAllTransactions();

    @Query("SELECT * FROM transactions ORDER BY tra_date DESC")
    LiveData<List<Transaction>> getAllTransactions();

    @TypeConverters(ConverterDate.class)
    @Query("SELECT * FROM transactions where tra_date between :sdate and :edate ORDER BY transaction_amount DESC")
    LiveData<List<Transaction>> getTransactionsByDate(Date sdate, Date edate);

    @Query("SELECT * FROM transactions WHERE account_from = :accountId")
    LiveData<List<Transaction>> getTransactionByAccountLD(int accountId);



    @Insert
    void setSchedule(Schedule schedule);

    @Delete
    void deleteSchedule(Schedule schedule);

    @Query("DELETE FROM schedule")
    void deleteAllSchedules();

    @Query("SELECT * FROM schedule")
    LiveData<List<Schedule>> getAllSchedules();
}
