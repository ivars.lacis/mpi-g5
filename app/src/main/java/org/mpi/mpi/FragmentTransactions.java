package org.mpi.mpi;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import org.mpi.mpi.db.types.Account;
import org.mpi.mpi.db.types.Transaction;
import org.mpi.mpi.helpers.RecyclerItemTouchHelper;
import org.mpi.mpi.utils.ADialog;
import org.mpi.mpi.views.TransactionRecyclerAdapter;
import org.mpi.mpi.views.TransactionViewModel;

import java.text.NumberFormat;
import java.util.List;

public class FragmentTransactions
        extends Fragment
        implements RecyclerItemTouchHelper.ITouchHelperListener, ADialog.IDialogResultListener {

    private static final String TAG = "FragmentTransactions";

    private TransactionViewModel mTransactionViewModel;
    private TransactionRecyclerAdapter mAdapter;
    private TextView mTotalBalance;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstancState) {

        View view = inflater.inflate(R.layout.fragment_transactions, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_transactions);
        mAdapter = new TransactionRecyclerAdapter(this, this.getContext());

        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));

        DividerItemDecoration div;
        switch (getResources().getConfiguration().orientation) {
            case 1:
                div = new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL);
                break;
            case 2:
                div = new DividerItemDecoration(view.getContext(), DividerItemDecoration.HORIZONTAL);
                break;
            default:
                div = new DividerItemDecoration(view.getContext(), DividerItemDecoration.VERTICAL);
        }
        recyclerView.addItemDecoration(div);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

        mTotalBalance = view.findViewById(R.id.total_balance);
        mTransactionViewModel = ViewModelProviders.of(FragmentTransactions.this).get(TransactionViewModel.class);
        mTransactionViewModel.getAllTransactions().observe(FragmentTransactions.this, new Observer<List<Transaction>>() {
            @Override
            public void onChanged(@Nullable final List<Transaction> transactions) {
                mAdapter.setTransactions(transactions);

                double totalBalance = 0;
                if (transactions != null) {
                    for (Transaction t : transactions) {
                        totalBalance += (t.getDirection().getValue() * t.getAmount());
                    }

                }
                String str = NumberFormat.getCurrencyInstance().format(totalBalance);
                mTotalBalance.setText(str);
            }
        });


        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), ActivityNewTransaction.class);
                startActivityForResult(i, 1);

            }
        });

        return view;
    }


    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof TransactionRecyclerAdapter.TransactionViewHolder) {
            Transaction i = mTransactionViewModel.getAllTransactions().getValue().get(viewHolder.getAdapterPosition());

            mAdapter.removeTransactions(viewHolder.getAdapterPosition());

            mTransactionViewModel.delete(i);

            Toast.makeText(this.getContext(), "Ieraksts dzēsts", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void receiveDialogResult(Object result) {
        Toast.makeText(this.getContext(), (String)result, Toast.LENGTH_LONG).show();

//        Transaction transaction = new Transaction(0.0);
//        mTransactionViewModel.setTransaction(transaction);
    }
}
