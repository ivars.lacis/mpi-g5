package org.mpi.mpi;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import org.mpi.mpi.db.types.Transaction;
import org.mpi.mpi.views.TransactionRecyclerAdapter;
import org.mpi.mpi.views.TransactionViewModel;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FragmentCalendar extends Fragment {
    private static final String TAG = "FragmentCalendar";

    private TransactionViewModel mTransactionViewModel;
    private TransactionRecyclerAdapter mAdapter;

    CalendarView mCalendarView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstancState) {

        View view = inflater.inflate(R.layout.fragment_calendar, container, false);

        RecyclerView recyclerView = view.findViewById(R.id.recycler_transactions_cal);
        mCalendarView = view.findViewById(R.id.cal_transactions);

        mAdapter = new TransactionRecyclerAdapter(this, this.getContext());

        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));


        Pair<Date, Date> dateInterval = getDateRange((new Date(mCalendarView.getDate())));
        mTransactionViewModel = ViewModelProviders.of(FragmentCalendar.this).get(TransactionViewModel.class);
        mTransactionViewModel.getTransactionsByDate(dateInterval.first, dateInterval.second).observe(FragmentCalendar.this, new Observer<List<Transaction>>() {
            @Override
            public void onChanged(@Nullable final List<Transaction> transactions) {
                mAdapter.setTransactions(transactions);
            }
        });


        mCalendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                Date d = new Date(year, month, dayOfMonth);
                Log.d(TAG, "onSelectedDayChange: date changed to" + d.toString());
            }
        });

        return view;
    }


    public Pair<Date, Date> getDateRange(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        Date sdate = cal.getTime();

        cal.set(Calendar.MILLISECOND, 999);
        cal.set(Calendar.SECOND, 59);
        cal.set(Calendar.MINUTE, 59);
        cal.add(Calendar.HOUR_OF_DAY, 23);
        Date edate = cal.getTime();

        return new Pair<>(sdate, edate);
    }
}
