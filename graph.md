```mermaid
graph TD
O[OverviewActivity]
ANTra[ActivityNewTransaction]
FAcc[FragmentAccounts]
FCal[FragmentCalendar]
FTra[FragmentTransactions]
FSch[FragmentScheduledTra]
O-->FTra
O-->FCal
O-->| pēc noklusējuma |FAcc
O-->FSch
FTra-->ANTra
style FSch stroke-dasharray: 5, 5
```